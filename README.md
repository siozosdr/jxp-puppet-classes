# README #

This repository contains Puppet classes implemented for the infrastructure of JourneyXP. They are divided in two categories: testing and production.
All classes are used with the Foreman UI.  Make sure foreman and puppet are setup. Add any of the classes to 
"/etc/puppet/labs/code/environments/production/modules" or "/etc/puppetlabs/code/modules", import them from foreman and apply them to the nodes

## Testing ##
Puppet classes: 

 - Apache for JourneyXP servers
 - CouchDB2.0, CouchDB2.1
 - Cron jobs for JourneyXP servers
 - MySQL server setup
 - RabbitMQ message broker
 - MySQL server setup(tailored to the needs of JourneyXP).
## Production ##
Puppet classes:

 - Couchdb 2.0
 - MySQL server setup
 - Puppet agent run interval