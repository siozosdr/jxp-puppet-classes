# Class: agent_runinterval
# ===========================
#
# Full description of class agent_runinterval here.
#
# Parameters
# ----------
#
# Document parameters here.
# * `runinterval`
# Set runinterval in seconds to trigger puppet agent run
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'agent_runinterval'}
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class agent_runinterval (
  $runinterval = 1200
) {
  file{'/etc/puppetlabs/puppet/puppet.conf':
    ensure => present,
  } ->
  #file_line{'Append runinterval to puppet.conf':
  #  path => '/etc/puppetlabs/puppet/puppet.conf',
  #  line => 'runinterval = $runinterval',
  #}
  file_line { 'Append agent runinterval to puppet.conf':
    path => '/etc/puppetlabs/puppet/puppet.conf',  
    line => "runinterval = ${runinterval}",
    match   => "^runinterval = .*",
  }
  #file_line { 'Append runinterval to puppet.conf':
  #  path => '/etc/puppetlabs/puppet/puppet.conf',  
  #  line => "runinterval = ${runinterval}",
  #  match   => "^    runinterval = .*",
  #}
}
