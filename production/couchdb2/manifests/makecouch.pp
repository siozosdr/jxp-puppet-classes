class couchdb2::makecouch (){
  # Fetches the couchdb2 data from the master node and places them under "/opt/couchdb2".
  require couchdb2::prepare
  file { "/opt/couchdb2.0.tar.gz":
       mode => "0644",
       #owner => 'root',
       #group => 'root',
       source => 'puppet:///modules/couchdb2/couchdb2.0.tar.gz',
  }
  staging::file{'couchdb2.0.tar.gz':
    source => '/opt/couchdb2.0.tar.gz'
  }
  staging::extract{'couchdb2.0.tar.gz':
    target => '/srv/',
    creates => '/srv/couchdb2',
    require => Staging::File['couchdb2.0.tar.gz']
  }
  # Replaced with the code above
  # Disabled because of unnecessary executions
  #exec { "/bin/tar -xvf /opt/couchdb2.tar.gz":
  #  cwd => "/opt/" ,
  #  creates => "/opt/couchdb2",
  #}
  #exec {"/bin/cp -r /opt/couchdb2 /srv/couchdb2":
  #  cwd => "/opt",
  #  creates => "/srv/couchdb2",
  #  returns => [0,1],
  #}
  #file { "/srv/apache-couchdb-2.0.0.tar.gz":
  #  source => "http://ftp.download-by.net/apache/couchdb/source/2.0.0/apache-couchdb-2.0.0.tar.gz",
  #  ensure => present,
  #}
  #exec { "/bin/tar -xvf /srv/apache-couchdb-2.0.0.tar.gz":
  #  cwd => "/srv/" ,
  #  creates => "/srv/apache-couchdb-2.0.0",
  #}
  #exec { "bash /srv/apache-couchdb-2.0.0/configure":
  #  cwd => "/srv/apache-couchdb-2.0.0/",
  #  path => "/usr/bin/:/usr/sbin:/bin/",
  #}
  #exec {'convert to deb':
  #  cwd => "/srv/apache-couchdb-2.0.0",
  #  command => "/usr/bin/checkinstall",
  #  creates => "/srv/apache-couchdb-2.0.0/apache-couchdb_2.0.0-1_amd64.deb",
  #  path => "/usr/bin/:/usr/sbin:/bin/", 
  #}
  #package {"couchdb2":
  #  provider => dpkg,
  #  ensure => installed,
  #  source => "/srv/apache-couchdb-2.0.0/apache-couchdb_2.0.0-1_amd64.deb"
  #}
  #exec { "bash /srv/apache-couchdb-2.0.0/configure && make -f /srv/apache-couchdb-2.0.0/Makefile release":
  #  cwd => "/srv/apache-couchdb-2.0.0/",
  #  path => "/usr/bin/:/usr/sbin:/bin/",
  #}
  #exec {'make couchdb':
  #  cwd => "/srv/apache-couchdb-2.0.0",
  #  path => "/usr/bin/:/usr/sbin:/bin/",
  #  command => "make release"
  #}
  #puppi::netinstall { 'couchdb2':
  #  url => 'http://ftp.download-by.net/apache/couchdb/source/2.0.0/apache-couchdb-2.0.0.tar.gz',
  #  destination_dir => '/srv/',
  #  postextract_cwd => '/srv/apache-couchdb-2.0.0/',
  #  postextract_command => 'bash /srv/apache-couchdb-2.0.0/configure && make release',
  #}
}
