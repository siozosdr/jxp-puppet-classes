class couchdb2::install {
  require couchdb2::makecouch 
  # Sets ups couchdb2 in "/opt/" directory
  #$check = $::checkdir
  #if $check == false {
    #notify{'Message': message => "check1 ${check}"}
    file{'/srv/couchdb2':
      path => '/srv/couchdb2',
      ensure => 'directory',
      #source => '/opt/couchdb2',
      owner => 'couchdb',
      #mode => '0777',
      group => 'couchdb',
      recurse => true,
    }
    # Replaced with the above
    #exec {"/bin/cp -r /srv/apache-couchdb-2.0.0/rel/couchdb /srv/couchdb2":
    #  cwd => "/srv/",
    #  creates => "/srv/couchdb2/",
    #  returns => [0,1],
    #}
    user {'CouchDB Administrator':
      name => 'couchdb',
      ensure => 'present',    
      system => 'true',
      shell => '/bin/bash',
    }
    # Replaced with the file types bellow
    #exec { 'Set ownership to couchdb':
    #   command  => "/bin/chown -R couchdb:couchdb /srv/couchdb2/",
    #   require  => File['/srv/couchdb2'],
    #   unless   => '/bin/ls -ld /srv/couchdb2 | /bin/grep "couchdb couchdb"',
    #}
    # Set file rights
    #file{'/srv/couchdb2/bin':
    #  path => '/srv/couchdb2/bin',
    #  ensure => 'directory',
    #  source => '/opt/couchdb2/bin',
    #  owner => 'couchdb',
    #  group => 'couchdb',
    #  recurse => true,
    #  mode => '0755',
    #}
    #file{'/srv/couchdb2/erts-7.3':
    #  path => '/srv/couchdb2/erts-7.3',
    #  ensure => 'directory',
    #  source => '/opt/couchdb2/erts-7.3',
    #  owner => 'couchdb',
    #  group => 'couchdb',
    #  recurse => true,
    #  mode => '0755',
    #}
    #file{'/srv/couchdb2/lib':
    #  path => '/srv/couchdb2/lib',
    #  ensure => 'directory',
    #  source => '/opt/couchdb2/lib',
    #  owner => 'couchdb',
    #  group => 'couchdb',
    #  recurse => true,
    #  mode => '0755',
    #}
    #file{'/srv/couchdb2/etc':
    #  ensure => 'directory',
    #  #path => "/srv/couchdb2/etc/",
    #  source => '/opt/couchdb2/etc',
    #  owner => 'couchdb',  
    #  group => 'couchdb',
    #  mode => "0755",
    #  recurse => true,
    #}
    #file{'/srv/couchdb2/etc/default.ini':
    #  ensure => 'present',
      #source => 'puppet:///modules/couchdb2/default.ini',
    #  mode => "0644",
    #  owner => 'couchdb',
    #  group => 'couchdb',
    #}
    #file{'/srv/couchdb2/etc/local.ini':
    #  ensure => 'present',
      #source => 'puppet:///modules/couchdb2/local.ini',
    #  mode => "0644",
    #  owner => 'couchdb',
    #  group => 'couchdb',
    #}
    #file{'/srv/couchdb2/etc/vm.args':
    #  ensure => 'present',
      #source => 'puppet:///modules/couchdb2/vm.args',
    #  mode => "0644",
    #  owner => 'couchdb',
    #  group => 'couchdb',  
    #}  
    # Replaced with the code above
    # Disabled because of unnecessary executions
    #exec {"usermod -d /srv/couchdb2 couchdb":
    #  path => "/usr/bin/:/usr/sbin:/bin",
    #}
    #exec {'Set ownership':
    #  command => "chown -R couchdb:couchdb /srv/couchdb2/",
    #  path => "/usr/bin/:/usr/sbin:/bin",
    #  returns => [0,1],
    #}
    # exec {'Set rights':
    #   command => "chmod 0644 /srv/couchdb2/etc/*",
    #  path => "/usr/bin/:/usr/sbin:/bin",
    #  returns => [0,1],
    #}
    #ini_setting {'chttpd setting change':
    #  ensure => present,
    #  section => 'chttpd',
    #  key_val_separator => '=',
    #  path => '/srv/couchdb2/etc/default.ini',
    #  setting => 'bind_address',
    #  value => '0.0.0.0',
    #  section_prefix => '[',
    #  section_suffix => ']',
    #}
    #ini_setting {'httpd setting change':
    #  ensure => present,
    #  section => 'httpd',
    #  key_val_separator => '=',
    #  path => '/srv/couchdb2/etc/default.ini',
    #  setting => 'bind_address',
    #  value => '0.0.0.0',
    #  section_prefix => '[',
    #  section_suffix => ']',
    #}
    #exec{'start CouchDB':
    #  command => "nohup  sudo -i -u couchdb /srv/couchdb2/bin/couchdb &",
    #  path => "/usr/bin/:/usr/sbin:/bin",
    #}
    
    #file_line { 'chttpd':
    #  path  => '/srv/couchdb2/etc/default.ini',
    #  line  => 'bind_address = 0.0.0.0',
    #  match => 'bind_address = 127.0.0.1',
    #}
    #file_line { 'httpd':
    #  path  => '/srv/couchdb2/etc/default.ini',
    #  line  => 'bind_address = 0.0.0.0',
    #  match => 'bind_address = 127.0.0.1',
    #}
    #file { "/srv/apache-couchdb-2.0.0.tar.gz":
    #  source => "http://ftp.download-by.net/apache/couchdb/source/2.0.0/apache-couchdb-2.0.0.tar.gz",
    #  ensure => present,
    #}
    #exec { "/bin/tar -xvf /srv/apache-couchdb-2.0.0.tar.gz":
    #  cwd => "/srv/" ,
    #  creates => "/srv/apache-couchdb-2.0.0",
    #}
    #exec { "./srv/apache-couchdb-2.0.0/configure":
    #  cwd => "/srv/apache-couchdb-2.0.0/",
    #}
  #} else {
  #  notify{'Message': message => "check2 ${check}"}
  #}
}  

