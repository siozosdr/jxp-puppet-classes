class couchdb2::daemon {
  # Sets up couchdb2 as a daemon
  require couchdb2::install
  $content = "[Unit]\nDescription=Couchdb service\nAfter=network.target\n\n[Service]\nType=simple\nUser=couchdb\nExecStart=/srv/couchdb2/bin/couchdb -o /dev/stdout -e /dev/stderr\nRestart=always\n\n[Install]\nWantedBy=multi-user.target\n"
  file {'Couchdb service file':
    path => "/etc/systemd/system/couchdb.service",
    ensure => present,
    content => $content,
    notify => Service['couchdb'],
  }
  service {'couchdb':
    ensure => 'running',
    enable => true,
  }
  # Replaced with the service resource above
  # Disabled because of unnecessary executions
  # exec {'reload daemon':
  #  command => "systemctl daemon-reload",
  #  path => "/bin/",
  #}
  # exec {'start service':
  #  command => "systemctl start couchdb.service",
  #  path => "/bin/",
  #}
  # exec {'enable service':
  #  command => "systemctl enable couchdb.service",
  #  path => "/bin/",
  #}
}
