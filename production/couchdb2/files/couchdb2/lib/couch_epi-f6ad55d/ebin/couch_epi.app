{application,couch_epi,
             [{description,"extensible plugin interface"},
              {vsn,"f6ad55d"},
              {registered,[couch_epi_sup,couch_epi_server]},
              {applications,[kernel,stdlib]},
              {mod,{couch_epi_app,[]}},
              {env,[{plugins,[couch_db_epi,chttpd_epi,couch_index_epi,
                              global_changes_epi,mango_epi,mem3_epi,
                              setup_epi]}]},
              {modules,[couch_epi,couch_epi_app,couch_epi_codechange_monitor,
                        couch_epi_codegen,couch_epi_data,couch_epi_data_gen,
                        couch_epi_functions,couch_epi_functions_gen,
                        couch_epi_module_keeper,couch_epi_plugin,
                        couch_epi_sup,couch_epi_util]}]}.
