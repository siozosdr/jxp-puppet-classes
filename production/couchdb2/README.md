# couchdb2

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with couchdb2](#setup)
    * [What couchdb2 affects](#what-couchdb2-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with couchdb2](#beginning-with-couchdb2)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
    * [Overview](#ref-overview)
    * [Structure](#ref-structure)
    * [Details](#ref-details)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This is a module in testing stage, that configures and installs CouchDB2 with default settings on Ubuntu 16.04 with Puppet 4.10 and registers it as a service.

## Setup

### What couchdb2 affects 

This module ensures that the following deb packages are installed on the system:

    build-essential, pkg-config, erlang, libicu-dev, libmozjs185-dev, libcurl4-openssl-dev, git

In addition, the CouchDB2 files are installed under the **/srv/couchdb2/** directory, and the couchdb user is created with the proper ownership rights over the folder. Finally, a service file at **/etc/systemd/system/couchdb.service** is created to register CouchDB2 as a service to be loaded at system boot.

If it's obvious what your module touches, you can skip this section. For
example, folks can probably figure out that your mysql_instance module affects
their MySQL instances.

### Setup Requirements
Modules: puppetlabs/inifile 

### Beginning with couchdb2

Add the module to the puppet modules directory and apply the class to a node either using a UI like Foreman,
or with PE:

    node /box/ {
      class {'::couchdb2':}
    }

Which will setup CouchDB2 on the target node with the user **node1** with the default configuration. Once the module is setup on an agent we need to access the CouchDB2 webfront at http://<IP-ADDRESS>:5984/_utils/ to perform the final configurations.

## Usage

Current version of the module takes no parameters.

E.g:
    class {'::couchdb2':}

Same result can be achieved using the Foreman UI. Just add the class to the desired puppet agent.

## Reference

### Overview:

The process of setting up CouchDB has been divided in 4 steps that need to be performed in a specific order. This order is:

	prepare -> makecouch -> install -> daemon

Puppet can give us the ability to define an order for the steps to take place. This was performed on this module with the _require_ keyword. As a result each step _requires_ the previous one to be executed successfuly, in order to be executed as well. As a result we can produce the desired order for a successful setup of CouchDB2.

### Structure:

	couchdb2/
	├── examples
	│   └── init.pp
	├── Gemfile
	├── manifests
	│   ├── daemon.pp
	│   ├── init.pp
	│   ├── install.pp
	│   ├── makecouch.pp
	│   ├── prepare.pp
	├── metadata.json
	├── Rakefile
	├── README.md
	└── spec
	    ├── classes
	    │   └── init_spec.rb
	    └── spec_helper.rb
	    
### Details:

#### couchdb2::prepare class: 
This class prepares the node with all the prerequisites needed to run CouchDB2 (build-essential, erlang, etc.).

	class couchdb2::prepare {
	  package {'build-essential':
	    ensure => installed,
	  }
	  package {'pkg-config':
	    ensure => installed,
	  }
	  package {'erlang':
	    ensure => installed,
	  }
	  package {'libicu-dev':
	    ensure => installed,
	  }
	  package {'libmozjs185-dev':
	    ensure => installed,
	  }
	  package {'libcurl4-openssl-dev':
	    ensure => installed,
	  }
	  package {'git':
	    ensure => installed,
	  }
	 
	  exec {"/usr/bin/git clone https://github.com/example42/puppi.git":
	    cwd => "/etc/puppetlabs/code/environments/production/modules/",
	    creates => "/etc/puppetlabs/code/environments/production/modules/puppi",
	  }
	  exec{"rm /usr/lib/erlang/man":
	    path => "/usr/bin/:/usr/sbin:/bin/",
	    returns => [0,1],
	  }
	}

#### couchdb2::makecouch class:
Sets up the the directories for CouchDB, installes the files and prepares them to be configured.

	class couchdb2::makecouch (
	  $username = $::couchdb2::user){
	  require couchdb2::prepare
	  # We can change the source of couchdb release version
	  # to download it from a static link on another server.
	  # The current configuration assumes that the file is in
	  # the Desktop of the user (e.g. /home/jxp/Desktop/couchdb2.tar.gz)
	  exec { "/bin/tar -xvf /home/$username/Desktop/couchdb2.tar.gz":
	    cwd => "/home/$username/Desktop/" ,
	    creates => "/home/$username/Desktop/couchdb2",
	  }
	  exec {"/bin/cp -r /home/$username/Desktop/couchdb2 /srv/couchdb2":
	    cwd => "/home/$username/Desktop/",
	    creates => "/srv/couchdb2",
	    returns => [0,1],
	  }
	}

#### couchdbd2::install class:
Sets rights and ownerships for specific files of CouchDB and sets up the CouchDB ini files.

	class couchdb2::install {
	  require couchdb2::makecouch
	  user {'CouchDB Administrator':
	    name => 'couchdb',
	    ensure => 'present',    
	    system => 'true',
	    shell => '/bin/bash',
	  }
	  exec {"usermod -d /srv/couchdb2 couchdb":
	    path => "/usr/bin/:/usr/sbin:/bin",
	  }
	  exec {'Set ownership':
	    command => "chown -R couchdb:couchdb /srv/couchdb2/",
	    path => "/usr/bin/:/usr/sbin:/bin",
	    returns => [0,1],
	  }
	  exec {'Set rights':
	    command => "chmod 0644 /srv/couchdb2/etc/*",
	    path => "/usr/bin/:/usr/sbin:/bin",
	    returns => [0,1],
	  }
	  ini_setting {'chttpd setting change':
	    ensure => present,
	    section => 'chttpd',
	    key_val_separator => '=',
	    path => '/srv/couchdb2/etc/default.ini',
	    setting => 'bind_address',
	    value => '0.0.0.0',
	    section_prefix => '[',
	    section_suffix => ']',
	  }
	  ini_setting {'httpd setting change':
	    ensure => present,
	    section => 'httpd',
	    key_val_separator => '=',
	    path => '/srv/couchdb2/etc/default.ini',
	    setting => 'bind_address',
	    value => '0.0.0.0',
	    section_prefix => '[',
	    section_suffix => ']',
	  }
	}


#### couchdb2::daemon class:
Sets up the service file for CouchDB and adds it to the list of services so we can query the service status

	class couchdb2::daemon {
	  require couchdb2::install
	  $content = "[Unit]\nDescription=Couchdb service\nAfter=network.target\n\n[Service]\nType=simple\nUser=couchdb\nExecStart=/srv/couchdb2/bin/couchdb -o /dev/stdout -e /dev/stderr\nRestart=always\n\n[Install]\nWantedBy=multi-user.target\n"
	  file {'Couchdb service file':
	    path => "/etc/systemd/system/couchdb.service",
	    ensure => present,
	    content => $content,
	  }
	  exec {'reload daemon':
	    command => "systemctl daemon-reload",
	    path => "/bin/",
	  }
	  exec {'start service':
	    command => "systemctl start couchdb.service",
	    path => "/bin/",
	  }
	  exec {'enable service':
	    command => "systemctl enable couchdb.service",
	    path => "/bin/",
	  }
	}

## Limitations

The file containing the class definition of	the module should have the name "init.pp" under the "manifests/" directory, so that it can be recognized by the "init.pp" file in the "examples/" directory. The "examples/init.pp" file ONLY includes the couchdb2 class. 

The module has only been tested on Ubuntu 16.04 with the prebuilt CouchDB2 present in the _Desktop_ directory of the node to be installed (e.g. '/home/nodename/Desktop/couchdb2.tar.gz')

## Development

Next development step can be the automation of the makefile execution of the CouchDB2 source code (**makecouch.pp** file). This way the module will not need a prebuilt version of CouchDB2.
