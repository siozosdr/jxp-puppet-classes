class mysql::mysql_jxp(
  $root_pass = UNDEF,
  $admin_pass = UNDEF,
  $override_opt = {}
){
  class { 'mysql::server':
    root_password => $root_pass,
    override_options => $override_opt,
  }

  mysql_user{ 'admin@%':
    ensure        => present,
    password_hash => mysql_password($admin_pass),
    require => Class['mysql::server'],
  }
}
