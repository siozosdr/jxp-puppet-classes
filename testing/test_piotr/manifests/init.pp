#
class test_piotr(
  $myclassname = '3',
){
 # notify {"Running with  ID defined": mesage => "This is a notify to make it clear"}

#dump all stuff from pupet to /tmp/yaml on !CLIENT!
 file { '/tmp/facts.yaml':
    content => inline_template('<%= scope.to_hash.reject { |k,v| !( k.is_a?(String) && v.is_a?(String) ) }.to_yaml %>'),
  }
deprecation("key", "message54tyriudkj76")
  notify { "w00t": message => "foo" }
  notify { "mynotice": }
  notice("A third notify just ot make sure")

  notify { 'FqdnTest':
    withpath => false,
    name     => "SHORTmy fqdn is xxx",
  }

notify { 'FqdnTestCANT be THE SAME NAME':
    withpath => true,
    name     => "LONGmy fqdn is xxx",
  }
 # fail("Magnificent fail message to stunt the puppet user")
}
