class apache::mod::php_apt {
  package { ['php']:
    provider => 'apt',
    ensure => 'present',
    install_options => ['--allow-unauthenticated', '-f'],
  }
}
