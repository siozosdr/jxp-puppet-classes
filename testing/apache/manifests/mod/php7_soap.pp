class apache::mod::php7_soap {
  package { 'php7.0-soap':
    provider => 'apt',
    ensure => 'present',
  }
}
