class apache::mod::php7_mbstring {
  package { 'php7.0-mbstring':
    provider => 'apt',
    ensure => 'present',
  }
}
