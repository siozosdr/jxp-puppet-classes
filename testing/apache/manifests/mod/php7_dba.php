class apache::mod::php7_dba {
  package { 'php7.0-dba':
    provider => 'apt',
    ensure => 'present',
  }
}
