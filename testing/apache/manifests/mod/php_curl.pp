class apache::mod::php_curl {
  package { 'php-curl':
    provider => 'apt',
    ensure => 'installed',
  }
}

