class apache::mod::php_mysql {
  require apache::mod::php_apt
  package { 'php-mysql':
    provider => 'apt',
    ensure => 'installed',
  }
}
