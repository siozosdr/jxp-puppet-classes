class apache::mod::php7_zip {
  package { 'php7.0-zip':
    provider => 'apt',
    ensure => 'present',
  }
}
