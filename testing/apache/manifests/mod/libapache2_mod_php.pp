class apache::mod::libapache2_mod_php {
  package { 'libapache2-mod-php7.0':
    provider => 'apt',
    ensure => 'present',
  }
}
