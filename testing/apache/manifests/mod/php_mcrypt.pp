class apache::mod::php_mcrypt {
  package { 'php-mcrypt':
    provider => 'apt',
    ensure => 'installed',
  }
}
