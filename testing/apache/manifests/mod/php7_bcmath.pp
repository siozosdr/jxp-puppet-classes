class apache::mod::php7_bcmath {
  package { 'php7.0-bcmath':
    provider => 'apt',
    ensure => 'present',
  }
}

