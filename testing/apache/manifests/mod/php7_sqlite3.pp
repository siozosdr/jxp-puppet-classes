class apache::mod::php7_sqlite3 {
  package { 'php7.0-sqlite3':
    provider => 'apt',
    ensure => 'installed',
  }
}
