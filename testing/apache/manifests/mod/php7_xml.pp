class apache::mod::php7_xml {
  package { 'php7.0-xml':
    provider => 'apt',
    ensure => 'present',
  }
}
