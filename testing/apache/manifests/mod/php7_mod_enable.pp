class apache::mod::php7_mod_enable {
  include ::apache::params
  require apache::mod::php_apt
  ::apache::mod { 'php7':
    package => "php7.0",
    lib => "libphp7.0.so",
  }
}
