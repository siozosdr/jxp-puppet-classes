class apache::mod::php7_bz2 {
  package { 'php7.0-bz2':
    provider => 'apt',
    ensure => 'present',
  }
}
