Puppet::Functions.create_function(:generate_string) do
  # functions are written under <module path>/lib/puppet/functions
    @doc = "Test function to be called by init class"
    def generate_string()
      # seeds for SHA1
      o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
      string = (0...100).map { o[rand(o.length)] }.join
      result = Digest::SHA1.hexdigest(Base64.encode64(Digest::SHA1.digest(string))).split(//)
      existing = {}
      # make sure each character in the result is unique
      final = ""
      result.each do |letter|
        unless existing.key?(letter)
	  final << letter
          existing[letter] = 1
	end
      end
      return (final)
    end
end


