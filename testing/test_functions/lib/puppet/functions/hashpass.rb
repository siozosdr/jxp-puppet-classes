Puppet::Functions.create_function(:hashpass) do
  # functions are written under <module path>/lib/puppet/functions
    ##@doc = "Test function to be called by init class"
    def hashpass(arg)
      return '' if arg.empty?
      return arg if arg =~ /\*[A-F0-9]{40}$/
      '*' + Digest::SHA1.hexdigest(Digest::SHA1.digest(arg)).upcase
    end
end
