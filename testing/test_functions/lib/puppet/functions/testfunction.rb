Puppet::Functions.create_function(:testfunction) do
  # function are written under <module path>/lib/puppet/functions
    
    #def testfunction(admin_hash, root_pass, clients)
    def testfunction(root_pass, clients)
      @doc = "Function that creates an array of JSON based on the input given"  
      final_array = Array.new
      main_u_array_pass = Array.new
      stats_u_array_pass = Array.new
      clients.each do |client|
  # example clients:
  # [clientname1 => {main_u => clientpass}, {stats_u => clientpass2},
  #  clientname2 => {main_u => clientpass}, {stats_u => clientpass2}]
        #main_u_generatedPass = call_function('generate_string')
	#main_u_pass_hashed = call_function('hashpass', client["main_u_password"])
	#stats_u_generatedPass = call_function('generate_string')
	#stats_u_pass_hashed = call_function('hashpass', client["stats_u_password"])
	# generate JSON string
        string = ""
        string << "{"
        #string << "\"admin_u_pass_hash\":\"" << admin_hash <<"\","
	string << "\"root_password\":\"" << root_pass << "\","
        string << "\"client_shortname\":\"" << client["name"] << "\","	
	#string << "\"main_u_pass_hash\":\"" << main_u_pass_hashed << "\","
        string << "\"main_u_pass_hash\":\"" << client["main_u_password"] << "\","
        #string << "\"stats_u_pass_hash\":\"" << stats_u_pass_hashed << "\","
        string << "\"stats_u_pass_hash\":\"" << client["stats_u_password"] << "\","
	string << "\"override_options\":{\"mysqld\":{\"bind_address\":\"0.0.0.0\", \"sql_mode\":\"NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\"
  }}"
	string << "}"
	# parse JSON into hash
	json_output = call_function('parsejson', string, {})
	# add hash to array
	final_array.push(json_output)
	#main_u_array_pass.push(main_u_generatedPass)
	#stats_u_array_pass.push(stats_u_generatedPass)
      end
      return final_array#, main_u_array_pass, stats_u_array_pass
    end
end

# Example Json output fields
#"admin_u_pass_hash":"*132CE0D9555F89B2C4AE313485CDDAF6A25D01BC",
#"client_shortname":"sokratis",
#"main_u_pass_hash":"*F5400A0BAF431CF7AE0D359A70A66823425E3DE9",
#"override_options":{
#  "mysqld"{
#    "bind_address":"0.0.0.0",
#"sql_mode":"NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
#  }
#},
#  "root_password":"test",
#  "stats_u_pass_hash":"*E2B1F97D425720F4E5CDC05A4C9FCA4D7939B010"
