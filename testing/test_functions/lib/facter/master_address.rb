# THIS IS EXECUTED ON AGENT SIDE

Facter.add("master_address") do
  # functions are written under <module path>/lib/puppet/functions
  setcode do
    "192.168.1.39"
  end
end
