require 'logger'
function test_functions::test($arg) >> Notify {
  case $arg {
    false: {notify{'false':}}
    true: {notify{'true'}}
    default: {notify{"$arg"}}
  }
}
