# mysql_jxp

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with mysql_jxp](#setup)
    * [What mysql_jxp affects](#what-mysql_jxp-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with mysql_jxp](#beginning-with-mysql_jxp)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)

## Description

This module sets up a MySQL instance for a JourneyXP client.

## Setup

### What mysql_jxp affects

This module affects MySQL users rights, databases and tables.
### Setup Requirements

mysql::server

### Beginning with mysql_jxp
```puppet
class { 'mysql_jxp':
  user_data => [
    {
      client_shortname => "sokratis",
      main_u_pass_hash => "*13ABCDE123456879",
      stats_u_pass_hash => "*13ABCDE123456879",
      admin_u_pass_hash => "*13ABCDE123456879",
      create_root_my_cnf => false,
      override_options => {},
      root_password => "test"
    },
    {
      client_shortname => "piotr",
      main_u_pass_hash => "*13ABCDE123456879",
      stats_u_pass_hash => "*13ABCDE123456879",
      admin_u_pass_hash => "*13ABCDE123456879",
      create_root_my_cnf => false,
      override_options => {},
      root_password => "test"
    },
  ],
}
```
## Usage
Using puppet you can call the class like below:
```puppet
class { 'mysql_jxp':
  user_data => [
    {
      client_shortname => "sokratis",
      main_u_pass_hash => "*13ABCDE123456879",
      stats_u_pass_hash => "*13ABCDE123456879",
      admin_u_pass_hash => "*13ABCDE123456879",
      create_root_my_cnf => false,
      override_options => {},
      root_password => "test"
    },
    {
      client_shortname => "piotr",
      main_u_pass_hash => "*13ABCDE123456879",
      stats_u_pass_hash => "*13ABCDE123456879",
      admin_u_pass_hash => "*13ABCDE123456879",
      create_root_my_cnf => false,
      override_options => {},
      root_password => "test"
    },
  ],
}
```
Otherwise you can use the Foreman UI to override the necessary smart class variables as shown in the examples above.

## Reference

### init.pp

External class to be used for a MySQL JourneyXP customer instance.

### base.pp
```puppet
define mysql_jxp::base(
  $user_data,
){
  if defined(Class['mysql::server']){
    $user_data.each |$user| {
      mysql_jxp::deployment {"mysql_jxp_deployment_${user[client_shortname]}":
        #title => "mysql_jxp_deployment_${user[client_shortname]}",
        client_shortname => $user[client_shortname],
        main_u_pass_hash => $user[main_u_pass_hash],
        stats_u_pass_hash => $user[stats_u_pass_hash],
        admin_u_pass_hash => $user[admin_u_pass_hash],
        create_root_my_cnf => $user[create_root_my_cnf],
        override_options => $user[override_options],
        root_password => $user[root_password],
      }
      #notify{"Databases for user ${client_shortname} have been created":}
    }
  } else {
    notify {"Missing class mysql::server. Nothing to do. Apply mysql::server class to this host.":}
  }
}
```
Internal class meant to be used by another puppet module. Contains the same code with init.pp.

### deployment.pp

Internal class meant to be used ONLY by the module to setup the MySQL instance based on the details given through the "user_data" variable.
```puppet
# Private class of the module, meant to be used ONLY internally.
define mysql_jxp::deployment(
  $client_shortname,
  $main_u_pass_hash,
  $stats_u_pass_hash,
  $admin_u_pass_hash,
  $create_root_my_cnf = false,
  $override_options,
  $root_password,
){
  if defined(Class['mysql::server']){
  #  mysql_user{ 'admin@%':
  #    ensure        => present,
  #    password_hash => $admin_u_pass_hash,
  #require       => Class['mysql::server'],
  #  }
  
    mysql_user{ "${client_shortname}_stats_u@%":
      ensure        => present,
      password_hash => $stats_u_pass_hash,
      require       => Class['mysql::server'],
    }
    
    mysql_user{ "${client_shortname}_main_u@%":
      ensure        => present,
      password_hash => $main_u_pass_hash,
      #require       => Class['mysql::server'],
    }
    
    mysql_grant{"${client_shortname}_main_u@%/${client_shortname}_main.*    ":
      user       => "${client_shortname}_main_u@%",
      table      => "${client_shortname}_main.*",
      privileges => ['ALL'],
    }

    mysql_grant{"${client_shortname}_main_u@%/jxptemp_${client_shortname}.*":
      user       => "${client_shortname}_main_u@%",
      table      => "jxptemp_${client_shortname}.*",
      privileges => ['ALL'],
    }
    mysql_grant{"${client_shortname}_stats_u@%/${client_shortname}_stats.*":
      user       => "${client_shortname}_stats_u@%",
      table      => "${client_shortname}_stats.*",
      privileges => ['ALL'],
    }
    
    mysql_database{ "${client_shortname}_main":
      ensure  => present,
      charset => 'utf8',
    }

    mysql_database{ "${client_shortname}_stats":
      ensure  => present,
      charset => 'utf8',
    }

    mysql_database{ "jxptemp_${client_shortname}":
      ensure  => present,
      charset => 'utf8',
    }
    } else {
      #fail(" \033[31mHello\e[0m World")
      #fail(" \n\n\n\n COOL FAIL (only once :) & remember space before first \\n)\n\n BTW Required mysql::server missing. Noting to do. Apply mysql::server class to this host.  \n\n\n\n")
      notify {"Missing class mysql::server. Nothing to do. Apply mysql::server class to this host.":}
    }
}
```

## Limitations

This module has ONLY been tested for Ubuntu 16.04.