# Private class of the module, meant to be used ONLY internally.
define mysql_jxp::deployment(
  $client_shortname,
  $main_u_pass_hash,
  $stats_u_pass_hash,
  $admin_u_pass_hash,
  $create_root_my_cnf = false,
  $override_options,
  $root_password,
){
  if defined(Class['mysql::server']){
#  mysql_user{ 'admin@%':
#    ensure        => present,
#    password_hash => $admin_u_pass_hash,
    #require       => Class['mysql::server'],
#  }

    mysql_user{ "${client_shortname}_stats_u@%":
      ensure        => present,
      password_hash => $stats_u_pass_hash,
      require       => Class['mysql::server'],
    }

    mysql_user{ "${client_shortname}_main_u@%":
      ensure        => present,
      password_hash => $main_u_pass_hash,
      #require       => Class['mysql::server'],
    }

    mysql_grant{"${client_shortname}_main_u@%/${client_shortname}_main.*":
      user       => "${client_shortname}_main_u@%",
      table      => "${client_shortname}_main.*",
      privileges => ['ALL'],
    }

    mysql_grant{"${client_shortname}_main_u@%/jxptemp_${client_shortname}.*":
      user       => "${client_shortname}_main_u@%",
      table      => "jxptemp_${client_shortname}.*",
      privileges => ['ALL'],
    }
    mysql_grant{"${client_shortname}_stats_u@%/${client_shortname}_stats.*":
      user       => "${client_shortname}_stats_u@%",
      table      => "${client_shortname}_stats.*",
      privileges => ['ALL'],
    }

    mysql_database{ "${client_shortname}_main":
      ensure  => present,
      charset => 'utf8',
    }

    mysql_database{ "${client_shortname}_stats":
      ensure  => present,
      charset => 'utf8',
    }

    mysql_database{ "jxptemp_${client_shortname}":
      ensure  => present,
      charset => 'utf8',
    }
  } else {
    #fail(" \033[31mHello\e[0m World")
     #fail(" \n\n\n\n COOL FAIL (only once :) & remember space before first \\n)\n\n BTW Required mysql::server missing. Noting to do. Apply mysql::server class to this host.  \n\n\n\n")
    notify {"Missing class mysql::server. Nothing to do. Apply mysql::server class to this host.":}
  }
}
