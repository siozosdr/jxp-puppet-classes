define mysql_jxp::base(
  $user_data,
){
  if defined(Class['mysql::server']){
    $user_data.each |$user| {
      mysql_jxp::deployment {"mysql_jxp_deployment_${user[client_shortname]}":
        #title => "mysql_jxp_deployment_${user[client_shortname]}",
        client_shortname => $user[client_shortname],
        main_u_pass_hash => $user[main_u_pass_hash],
        stats_u_pass_hash => $user[stats_u_pass_hash],
        admin_u_pass_hash => $user[admin_u_pass_hash],
        create_root_my_cnf => $user[create_root_my_cnf],
        override_options => $user[override_options],
        root_password => $user[root_password],
      }
      #notify{"Databases for user ${client_shortname} have been created":}
    }
  } else {
    notify {"Missing class mysql::server. Nothing to do. Apply mysql::server class to this host.":}
  }
}
