# Class: couchdb21
# ===========================
#
# Full description of class couchdb21 here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'couchdb21':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class couchdb21(
  String $admin_pass,
) {
  #apt::key {'HEXKEYID':
  #  source => 'https://www.dotdeb.org/dotdeb.gpg',
  #  id     => 'ID_OF_YOUR_KEY'
  #}
  include apt
  apt::source { 'couchdb':
    comment  => 'This is the official Apache CouchDB 2.1 mirror.',
    location => 'https://apache.bintray.com/couchdb-deb',
    release  => 'xenial',
    repos    => 'main',
    key => {
      'id' => 'D401AB61',
      'options' => 'https://couchdb.apache.org/repo/bintray-pubkey.asc',
    },
    #key      => 'D401AB61',
    #key_server => 'https://couchdb.apache.org/repo/bintray-pubkey.asc',
  }
  package{'couchdb':
    ensure => present,
    install_options => ['--allow-unauthenticated', '-f'],
    require => [Apt::Source['couchdb']]
  }
  ini_setting {'chttpd setting change port':
    ensure => present,
    section => 'chttpd',
    key_val_separator => '=',
    path => '/opt/couchdb/etc/local.ini',
    setting => 'port',
    value => '5984',
    section_prefix => '[',
    section_suffix => ']',
  }
  ini_setting {'chttpd setting change bind_address':
    ensure => present,
    section => 'chttpd',
    key_val_separator => '=',
    path => '/opt/couchdb/etc/local.ini',
    setting => 'bind_address',
    value => '0.0.0.0',
    section_prefix => '[',
    section_suffix => ']',
  }
  $check = $::couchdb_lock_exists
  if $check == false {
    file_line { 'admin user':
      ensure => present,
      path   => '/opt/couchdb/etc/local.ini',
      line   => "admin = ${admin_pass}",
      match  => '^admin\ =',
    }
    # file used to set the admin password only ONCE
    file{"/opt/couchdb/passwordLock":
      ensure => present,
      owner   => 'root',
      mode    => '0400',
    }
  }
  service {'couchdb':
    ensure => 'running',
    enable => true,
  }
  
}
