# THIS IS EXECUTED ON AGENT SIDE
def couchdb_lock_exists(file)
#    res = Pathname(file).exist?()
    res = File.exist?(file)   
    return res
end

Facter.add("couchdb_lock_exists") do
  # File is written under <module path>/lib/facter
  setcode do
    couchdb_lock_exists('/opt/couchdb/passwordLock')
  end
end
  

