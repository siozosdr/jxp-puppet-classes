# THIS IS EXECUTED ON AGENT SIDE
def checkdir(dir)
#    res = Pathname(file).exist?()
    res = File.directory?(dir)   
    return res
end

Facter.add("file_exists") do
  # File is written under <module path>/lib/facter
  setcode do
    checkdir('/srv/couchdb2')
  end
end
  

