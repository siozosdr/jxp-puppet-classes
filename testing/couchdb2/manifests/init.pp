# Class: couchdb2
# ===========================
#
# This class only requires that the couchdb2::daemon class is implemented. This 
# will mean that the setup of the module was successful and therefore finish.
# Examples
# --------
#
# @example
#    include ::couchdb2
#
# Authors
# -------
#
# Sokratis Siozos-Drosos ssd@journeyxp.com
#
# Copyright
# ---------
#
# Apache License 2.0
class couchdb2 () {
  require couchdb2::daemon
  #Class['couchdb2::step1']-> Class['couchdb2::step2'] 
}
