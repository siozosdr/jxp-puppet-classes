class couchdb2::prepare {
  package {'build-essential':
    ensure => installed,
  }
  package {'pkg-config':
    ensure => installed,
  }
  package {'erlang':
    ensure => 'latest',
  }
  package {'libicu-dev':
    ensure => installed,
  }
  package {'libmozjs185-dev':
    ensure => installed,
  }
  package {'libcurl4-openssl-dev':
    ensure => installed,
  }
  package {'git':
    ensure => installed,
  }
  #package {'checkinstall':
  #  ensure => installed,
  #}
  # Disabled because of unnecessary executions
  # exec {"/usr/bin/git clone https://github.com/example42/puppi.git":
  #  cwd => "/etc/puppetlabs/code/environments/production/modules/",
  #  creates => "/etc/puppetlabs/code/environments/production/modules/puppi",
  #}
 #exec{"rm /usr/lib/erlang/man":
 #   path => "/usr/bin/:/usr/sbin:/bin/",
 #   returns => [0,1],
 # }
  #file { "/srv/apache-couchdb-2.0.0.tar.gz":
  #  source => "http://ftp.download-by.net/apache/couchdb/source/2.0.0/apache-couchdb-2.0.0.tar.gz",
  #  ensure => present,
  #}
  #exec { "/bin/tar -xvf /srv/apache-couchdb-2.0.0.tar.gz":
  #  cwd => "/srv/" ,
  #  creates => "/srv/apache-couchdb-2.0.0",
  #}
}
