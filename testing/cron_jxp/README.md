# cron_jxp

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with cron_jxp](#setup)
    * [What cron_jxp affects](#what-cron_jxp-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with cron_jxp](#beginning-with-cron_jxp)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module sets up the default cron jobs for a JourneyXP client along with any extra jobs given through the parameters.

## Setup

### Beginning with cron_jxp

For a default installation of the module simply add the class to the desired agent node through foreman with the client_name parameter given and the default FIVE cron jobs will be added to crontab.
e.g.:
```puppet
class {'cron_jxp':
  client_name => "sokratis",
}
```
## Usage

To add extra cron jobs apart from the default 5 of a client you need to override the "extra_jobs" variable, which accepts an Array of Hashes. For an example structure of extra jobs look inside the "init.pp" file.

e.g.:
```puppet	
class {'cron_jxp':
  client_name => "sokratis",
  extra_jobs => [
    {
      command     => '/usr/bin/sleep 1',
    },
    {
      command     => '/usr/bin/sleep 10',
      special     => 'reboot',
    },
  ]
}
```
## Reference

### cron_jxp class:
```puppet
class cron_jxp (
  $client_name,
  $extra_jobs = [],
  $environment = [ 'PATH="/usr/sbin:/usr/bin:/sbin:/bin"' ],  
){
  $cmd1 = "wget -O /dev/null http://${client_name}.journeyxp.com/journeys/sendAllJourneys > /dev/null 2>&1 &"
  $cmd2 = "wget -O /dev/null http://${client_name}.journeyxp.com/statistics/calcYesterdayStats/ChickiBikiSky2015 > /dev/null 2>&1 &"
  $cmd3 = "wget -O /dev/null http://${client_name}.journeyxp.com/executioner/submit_csv_leads > /dev/null 2>&1 &"
  $cmd4 = "wget -O /dev/null http://${client_name}.journeyxp.com/statistics/todayDashboardStats/ChickiBiniSky2015 > /dev/null 2>&1 & > /dev/null"
  $cmd5 = "wget -O /dev/null http://${client_name}.journeyxp.com/scheduler/run > /dev/null 2>&1 &"
  $jobs = [
    {
       minute      => '*/7',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => "wget -O /dev/null http://${client_name}.journeyxp.com/journeys/sendAllJourneys > /dev/null 2>&1 &",
    },
    {
       minute      => '3',
       hour        => '0',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd2,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd3,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd4,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd5,
    }
  ]
  $total_jobs = unique(flatten([$jobs, $extra_jobs]))
  cron::job::multiple{'jxp_cronjobs':
    jobs => $total_jobs,
    environment => $environment,
  }
  
}
```

## Limitations

This module has ONLY been tested for Ubuntu 16.04
