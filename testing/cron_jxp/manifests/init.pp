# Class: cron_jxp
# ===========================
#
# Full description of class cron_jxp here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `client_name`
# Name to be used as part of the cron job commands
# * `extra_jobs`
# Extra jobs to be added to the agent apart from the 5 default used by a JXP client
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'cron_jxp':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class cron_jxp (
  $client_name,
  $extra_jobs = [],
# extrajobs example:
#   $extra_jobs = [
#     {
#       minute      => '55',
#       hour        => '5',
#       date        => '*',
#       month       => '*',
#       weekday     => '*',
#       user        => 'rmueller',
#       command     => '/usr/bin/uname',
#     },
#     {
#       command     => '/usr/bin/sleep 1',
#     },
#     {
#       command     => '/usr/bin/sleep 10',
#       special     => 'reboot',
#     },
#   ]

#  $name, # (namevar) The symbolic name of the cron job.  This name is 
#  $ensure      = 'present',# The basic property that the resource should be...
#  $command, # The command to execute in the cron job.  The... 
#  $minute      = '*',# The minute at which to run the cron job...
#  $hour        = '*',# The hour at which to run the cron job. Optional;
#  $monthday    = '*', # The day of the month on which to run the...
#  $month       = '*',# The month of the year.  Optional; if specified...
#  $weekday     = '*', # The weekday on which to run the command...

#  $environment = [],# Any environment settings associated with this...
#  $provider    = 'crontab', # The specific backend to use for this `cron...
#  $special     = undef, # A special value such as 'reboot' or 'annually'...
#  $target      = undef, # The name of the crontab file in which the cron...
#  $user        = 'root', # The user who owns the cron job.  This user must...
  
  $environment = [ 'PATH="/usr/sbin:/usr/bin:/sbin:/bin"' ],  
){
  $cmd1 = "wget -O /dev/null http://${client_name}.journeyxp.com/journeys/sendAllJourneys > /dev/null 2>&1 &"
  $cmd2 = "wget -O /dev/null http://${client_name}.journeyxp.com/statistics/calcYesterdayStats/ChickiBikiSky2015 > /dev/null 2>&1 &"
  $cmd3 = "wget -O /dev/null http://${client_name}.journeyxp.com/executioner/submit_csv_leads > /dev/null 2>&1 &"
  $cmd4 = "wget -O /dev/null http://${client_name}.journeyxp.com/statistics/todayDashboardStats/ChickiBiniSky2015 > /dev/null 2>&1 & > /dev/null"
  $cmd5 = "wget -O /dev/null http://${client_name}.journeyxp.com/scheduler/run > /dev/null 2>&1 &"
  $jobs = [
    {
       minute      => '*/7',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => "wget -O /dev/null http://${client_name}.journeyxp.com/journeys/sendAllJourneys > /dev/null 2>&1 &",
    },
    {
       minute      => '3',
       hour        => '0',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd2,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd3,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd4,
    },
    {
       minute      => '*/10',
       hour        => '*',
       date        => '*',
       month       => '*',
       weekday     => '*',
       user        => 'root',
       command     => $cmd5,
    }
  ]
  $total_jobs = unique(flatten([$jobs, $extra_jobs]))
  cron::job::multiple{'jxp_cronjobs':
    jobs => $total_jobs,
    environment => $environment,
  }
  
}
