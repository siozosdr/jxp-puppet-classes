class { 'rabbitmq::repo::apt':
  pin => '900',
}->
class { 'rabbitmq::server':
  delete_guest_user => true,
#  version           => '2.4.1',
}->
rabbitmq_user { 'ssd':
  admin    => true,
  password => 'pass',
  provider => 'rabbitmqctl',
}->
rabbitmq_vhost { '/':
  provider => 'rabbitmqctl',
}
rabbitmq_user_permissions { 'ssd@%':
  configure_permission => '.*',
  read_permission      => '.*',
  write_permission     => '.*',
  provider             => 'rabbitmqctl',
}
