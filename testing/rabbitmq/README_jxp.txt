## Overview

This module manages and installs RabbitMQ (www.rabbitmq.com). In addition, it imports the settings of a previous RabbitMQ installation to the new one. 

## Setup

### What rabbitmq affects

* rabbitmq repository files.
* rabbitmq package.
* rabbitmq configuration file.
* rabbitmq service.
* erlang package

### Beginning with rabbitmq


```puppet
include '::rabbitmq::servermodule'
```

The manifest file needed to setup a JourneyXP RMQ server is called "servermodule.pp". This profile class uses the Puppetlabs Puppet Forge's version of RabbitMQ to set up a basic installation, then imports settings from another RMQ installation to the new one. The file needed to import the settings should be at "/opt/def.json", where "def.json" must be the name of the JSON file containing the RabbitMQ settings we wish to import.

### Module dependencies

If running CentOS/RHEL, and using the yum provider, ensure the epel repo is present.

To have a suitable erlang version installed on RedHat and Debian systems,
you have to install another two puppet modules from http://forge.puppetlabs.com/ with:

    puppet module install garethr-erlang
    puppet module install puppetlabs/apt

This module handles the packages for erlang.
To use the module, add the following snippet to your site.pp or an appropriate profile class:

For RedHat systems:

    include 'erlang'
    class { 'erlang': epel_enable => true}

For Debian systems:

    include 'erlang'
    package { 'erlang-base':
      ensure => 'latest',
    }
