class rabbitmq::servermodule(
  $import_settings = false,
  $username = 'jxprmq',
  $userpassword = 'jxp',
){
  #class { 'rabbitmq::repo::apt':
  #  pin => '900',
  #}->
  include 'erlang'
  package { 'erlang-base':
    ensure => 'latest',
  }
  package {'curl':
    provider => 'apt',
    ensure => installed,
  }
  class {'rabbitmq':
    notify => Rabbitmq_user["${username}"],
  } ->
  #class { 'rabbitmq::server':
  #  #delete_guest_user => true,
  #  service_manage => true,
  #  service_ensure => 'running',
  #  version           => '2.4.1',
  #}->
  rabbitmq_user { "${username}":
    admin    => true,
    password => $userpassword,
    provider => 'rabbitmqctl',
  }->
  # Disabled for unnecessary executions/ management plugin enabled by default
  #exec{"rabbitmq-plugins enable rabbitmq_management":
  #  path => "/usr/bin:/sbin:/bin:/usr/sbin",
  #  returns => [0],
  #  refreshonly => true,
  #  subscribe => Package['erlang'],
  #} ->
  if $import_settings == true { # this case needs more testing
    file{'/opt/def.json':
      ensure => present,
    } ->
    exec{'Post new RMQ schema':
      command => "curl -i -u ssd:jxp -H \"content-type:application/json\" -X POST --data @/opt/def.json  http://localhost:15672/api/definitions",
      path => "/usr/bin:/sbin:/bin:/usr/sbin",
      require => File['/opt/def.json'],
    }
  }
  ini_setting {'file descriptors setting':
    ensure => present,
    section => 'Service',
    key_val_separator => '=',
    path => '/lib/systemd/system/rabbitmq-server.service',
    setting => 'LimitNOFILE',
    value => 'infinity',
    section_prefix => '[',
    section_suffix => ']',
  } ->
  ini_setting {'memory limit setting':
    ensure => present,
    section => 'Service',
    key_val_separator => '=',
    path => '/lib/systemd/system/rabbitmq-server.service',
    setting => 'LimitMEMLOCK',
    value => 'infinity',
    section_prefix => '[',
    section_suffix => ']',
  }
  #rabbitmq_policy {'mirror-all@.*':
  #  pattern => '^.*.',
  #  priority => 0,
  #  applyto => all,
  #  definition => {
  #    'ha-mode' => 'all',
  #    'ha-sync-mode' => 'automatic',
  #  },
  #}
  # Replaced by the above rabbitmq_policy
  #exec{'Apply policy':
  #  command => "rabbitmqctl set_policy mirror-all \"^.*.\" '{\"ha-mode\":\"all\",\"ha-sync-mode\":\"automatic\"}' ",
  #  path => "/usr/bin:/sbin:/bin:/usr/sbin",
  #} ->
  # Disabled for unnecessary executions
  # exec{'reload daemon':
  #  command => "systemctl daemon-reload",
  #  path => "/usr/bin:/sbin:/bin:/usr/sbin",
  #  require => Exec['Apply policy'],
  #  refreshonly => true,
  #} ->
  # Replaced with the 'service_ensure => running' resource above, to prevent unnecessary executions
  #exec{'restart server':
  #  command => "service rabbitmq-server restart",
  #  path => "/usr/bin:/sbin:/bin:/usr/sbin",
  #  require => Exec['reload daemon'],
  #  refreshonly => true,
  #}
}